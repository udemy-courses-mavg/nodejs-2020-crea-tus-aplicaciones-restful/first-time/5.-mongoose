const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/carsdb', {useNewUrlParser: true})
    .then(()=> console.log('Conectado correctamente a MongoDb'))
    .catch(()=> console.log('Error al conectarse a MongoDb'))

const carSchema = new mongoose.Schema({
    company: String,
    model: String,
    price: Number,
    year: Number,
    sold: Boolean,
    extras: [String],
    date: {type: Date, default: Date.now}
})

const Car = mongoose.model('car', carSchema)

//createCar()
//getCars()
//getCompanyAndFilterCars()
//getMoreFilterCar()
//getFilterPriceCar()
//getFilterPriceInNinCar()
//getFilterPriceAndOrCar()
//getCountCar()
//getPaginationCars()
//updateCar('5e10e909aaabde4278ff5dd7')
//updateFirstCar('5e10f2189c9b47441896895f')
deleteCar('5e10f84626d072903cfa9b2e')

async function deleteCar(id){
    const result = await Car.deleteOne({_id: id})
    console.log(result)
}


async function updateFirstCar(id){
    const result = await Car.update(
        {_id: id},
        {
            $set: {
                company: 'Seat',
                model: 'Ibiza'
            }
        }
    )
    
    console.log(result)
}


async function updateCar(id){
    const car = await Car.findById(id)
    if(!car) return
    
    car.company = 'Mercedes'
    car.model = 'Clase A'

    const result = await car.save()
    console.log(result)
}


async function getPaginationCars(){
    const pageNumber = 1
    const pageSize = 2

    const cars = await Car
        .find({})
        // .skip -> Devuelve todos los resultados de la busqueda pero iniciando del número de parametro indicado
        // Para este caso decimos que cada página esta conformada por dos documentos de la colección de la base de datos
        .skip((pageNumber-1)*pageSize) 
        .limit(pageSize)
        
    console.log(cars)
}


async function getCountCar(){
    const cars = await Car
        .find({company: "Audi"})
        .count()
    console.log(cars)
}


async function getFilterPriceAndOrCar(){
    const cars = await Car
        .find({})
        //.and([{company: 'Audi'}, {mdoel: 'X3'}])
        .or([{company: 'Audi'}, {model: 'X3'}])
    console.log(cars)
}


async function getFilterPriceInNinCar(){
    const cars = await Car
        .find({extras: {$in: 'Laser Light'}})
    console.log(cars)
}


async function getFilterPriceCar(){
    const cars = await Car
        .find({price: {$gte: 3500, $lt: 100000}})
    console.log(cars)
}

async function getMoreFilterCar(){
    const cars = await Car
        .find({company: 'BMW', sold: false})
        .sort({price: 1})
        .limit(2)
        .select({company: 1, model: 1, price: 1})
    console.log(cars)
}


async function getCompanyAndFilterCars(){
    const cars = await Car.find({company: 'BMW', sold: false})
    console.log(cars)
}


async function getCars(){
    const cars = await Car.find()
    console.log(cars)
}


async function createCar(){
    const car = new Car({        
        company: 'BMW',
        model: 'X3',
        price: 7500,
        year: 2019,
        sold: false,
        extras: ['Automatic', 'Laser Light', '4x4']
    })

    const result = await car.save()
    console.log(result)
}